package com.forth.gulimall.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.forth.common.utils.PageUtils;
import com.forth.gulimall.order.entity.PaymentInfoEntity;

import java.util.Map;

/**
 * 支付信息表
 *
 * @author fourth-miaowei
 * @email 2608957980@qq.com
 * @date 2020-11-12 09:13:18
 */
public interface PaymentInfoService extends IService<PaymentInfoEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

