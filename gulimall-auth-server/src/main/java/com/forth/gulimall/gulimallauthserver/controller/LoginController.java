package com.forth.gulimall.gulimallauthserver.controller;

import com.alibaba.fastjson.TypeReference;
import com.forth.common.constant.AuthServerConstant;
import com.forth.common.utils.R;
import com.forth.gulimall.gulimallauthserver.feign.MemberFeignService;
import com.forth.gulimall.gulimallauthserver.feign.ThirdPartFeignService;
import com.forth.common.vo.MemberRespVo;

import com.forth.gulimall.gulimallauthserver.vo.UserLoginVo;
import com.forth.gulimall.gulimallauthserver.vo.UserRegistVo;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Controller;


import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Controller
public class LoginController {

    @Autowired
    ThirdPartFeignService thirdPartFeignService;
    //注入redis 操作工具类
    @Autowired
    StringRedisTemplate redisTemplate;
    @Autowired
    MemberFeignService memberFeignService;

    @ResponseBody
    @GetMapping("/sms/sendcode")
    public R SendCode(@RequestParam("phone") String phone) {
        //1,接口防刷
        String redisCode = redisTemplate.opsForValue().get(AuthServerConstant.SMS_CODE_CACHE_PREFIX + phone);

        if (!StringUtils.isEmpty(redisCode)) {
            long l = Long.parseLong(redisCode.split("_")[1]);
            if (System.currentTimeMillis() - l < 60000) {
                //60秒不能再发
                return R.error(1111,"60秒不能再发");
            }
        }

        //String code = UUID.randomUUID().toString().substring(0, 5);
        String code = "12345";
        String code2 = code + "_" + System.currentTimeMillis();

        //2，验证码的再次验证  存 key -phone value-code
        //redis 缓存
        redisTemplate.opsForValue().set(AuthServerConstant.SMS_CODE_CACHE_PREFIX + phone, code2, 1000, TimeUnit.MINUTES);
        thirdPartFeignService.sendCode(phone, code);
        return R.ok();
    }

    /**
     *RedirectAttributes attributes  重定向
     * @param vo
     * @param result
     * @param attributes
     * @return
     */
    @PostMapping("/regist")
    public String regist(@Valid UserRegistVo vo, BindingResult result, RedirectAttributes attributes) {
        //注册成功回到登录
        if (result.hasErrors()) {
            //创建map

            /**
             * .map(fieldError -> {
             *                  String field = fieldError.getField();
             *                  String defaultMessage = fieldError.getDefaultMessage();
             *                  error.put(field,defaultMessage);
             *                  return
             *              })
             */
            Map<String, String> errors = result.getFieldErrors().stream().collect(Collectors.toMap(FieldError::getField, FieldError::getDefaultMessage));

            System.out.println(errors);
           // model.addAttribute("errors", errors);
            attributes.addFlashAttribute("errors", errors);
            //return "redirect:http://localhost:20000/reg.html";
            return "redirect:http://localhost:20000/reg.html";
        }


                //注册
        String code = vo.getCode();

        String s = redisTemplate.opsForValue().get(AuthServerConstant.SMS_CODE_CACHE_PREFIX + vo.getPhone());
        if (!StringUtils.isEmpty(s)){

            if (code.equals(s.split("_")[0])){
                //删除
                //edisTemplate.delete(AuthServerConstant.SMS_CODE_CACHE_PREFIX + vo.getPhone());
                System.out.println("---------"+111);
                R r = memberFeignService.regist(vo);
                System.out.println("----------------"+r.toString());

                        if(r.get("code").equals(0)){

                            return "redirect:http://localhost:20000/index.html";
                        }else{
                            Map<String, String> errors = new HashMap<>();
                            errors.put("msg",r.toString());
                            attributes.addFlashAttribute("errors", errors);
                            return "redirect:http://localhost:20000/reg.html";
                        }


            }else{
                Map<String, String> errors = new HashMap<>();
                System.out.println("---------------1");
                errors.put("code","验证码错误");
                attributes.addFlashAttribute("errors", errors);
                return "redirect:http://localhost:20000/reg.html";
            }
        }else{
            Map<String, String> errors = new HashMap<>();
            System.out.println("---------------2");
            errors.put("code","验证码错误");
            attributes.addFlashAttribute("errors", errors);

            return "redirect:http://localhost:20000/reg.html";
        }


    }
@PostMapping("/login")
public  String login(UserLoginVo vo, RedirectAttributes redirectAttributes, HttpSession session){
    System.out.println("------------"+vo);
        //远程登录
    R login = memberFeignService.login(vo);
    System.out.println("-----"+login.toString());
    if(login.get("code").equals(0)){
        MemberRespVo data = login.getData("data", new TypeReference<MemberRespVo>() {
        });
        session.setAttribute(AuthServerConstant.LOGIN_USER,data);
        System.out.println("----"+session.getAttribute(AuthServerConstant.LOGIN_USER));

        return "redirect:http://localhost:30000";
    }else{
        Map<String,String> errors =new HashMap<>();
        errors.put("msg","账号密码错误");
        redirectAttributes.addFlashAttribute("errors",errors);
        return "redirect:http://localhost:20000/index.html";
    }


}



}
