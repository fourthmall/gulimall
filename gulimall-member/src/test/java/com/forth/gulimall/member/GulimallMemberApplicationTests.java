package com.forth.gulimall.member;

import com.forth.gulimall.member.entity.MemberEntity;
import com.forth.gulimall.member.service.MemberService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class GulimallMemberApplicationTests {

	@Autowired
	MemberService memberService;

	@Test
	void contextLoads() {
		MemberEntity memberEntity = new MemberEntity();
		memberEntity.setCity("新乡");
		memberService.save(memberEntity);
		System.out.println("success");
	}

}
