package com.forth.gulimall.member.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.forth.common.utils.PageUtils;
import com.forth.gulimall.member.entity.MemberEntity;
import com.forth.gulimall.member.exception.PhoneExistException;
import com.forth.gulimall.member.exception.UsernameExistException;
import com.forth.gulimall.member.vo.MemberLoginVo;
import com.forth.gulimall.member.vo.MemberRegistVo;
import com.forth.gulimall.member.vo.SocialUserVo;

import java.util.Map;

/**
 * 会员
 *
 * @author fourth-miaowei
 * @email 2608957980@qq.com
 * @date 2020-11-12 09:30:56
 */
public interface MemberService extends IService<MemberEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void regist(MemberRegistVo vo);

    void checkPhoneUnique(String phone) throws PhoneExistException;
    void checkUsernameUnique(String Username) throws UsernameExistException;

    MemberEntity login(MemberLoginVo vo);

    MemberEntity login(SocialUserVo svo) throws Exception;
}

