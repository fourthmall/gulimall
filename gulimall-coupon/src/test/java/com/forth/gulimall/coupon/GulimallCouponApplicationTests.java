package com.forth.gulimall.coupon;

import com.forth.gulimall.coupon.entity.CouponEntity;
import com.forth.gulimall.coupon.service.CouponService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class GulimallCouponApplicationTests {

	@Autowired
	CouponService couponService;

	@Test
	void contextLoads() {
		CouponEntity couponEntity = new CouponEntity();
		couponEntity.setCode("123456");
		couponService.save(couponEntity);
		System.out.println("success");
	}

}
