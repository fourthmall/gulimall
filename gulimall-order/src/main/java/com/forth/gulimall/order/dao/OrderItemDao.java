package com.forth.gulimall.order.dao;

import com.forth.gulimall.order.entity.OrderItemEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 订单项信息
 * 
 * @author fourth-miaowei
 * @email 2608957980@qq.com
 * @date 2020-11-12 09:13:19
 */
@Mapper
public interface OrderItemDao extends BaseMapper<OrderItemEntity> {
	
}
