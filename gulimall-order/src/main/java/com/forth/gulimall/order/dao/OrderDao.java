package com.forth.gulimall.order.dao;

import com.forth.gulimall.order.entity.OrderEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 订单
 * 
 * @author fourth-miaowei
 * @email 2608957980@qq.com
 * @date 2020-11-12 09:13:19
 */
@Mapper
public interface OrderDao extends BaseMapper<OrderEntity> {
	
}
