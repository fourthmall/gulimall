package com.forth.gulimall.product.feign;

import com.forth.common.utils.R;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@FeignClient("ware")
public interface WareFeignService {

    @PostMapping("/hasstock")
    R getSkuHasStock(@RequestBody List<Long> skuIds);
}
