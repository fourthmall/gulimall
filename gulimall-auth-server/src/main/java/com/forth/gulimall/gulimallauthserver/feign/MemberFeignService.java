package com.forth.gulimall.gulimallauthserver.feign;

import com.forth.common.utils.R;

import com.forth.gulimall.gulimallauthserver.vo.SocialUserVo;
import com.forth.gulimall.gulimallauthserver.vo.UserLoginVo;
import com.forth.gulimall.gulimallauthserver.vo.UserRegistVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient("member")
public interface MemberFeignService {

    @PostMapping("member/member/regist")
    R regist(@RequestBody UserRegistVo vo) ;

    @PostMapping("member/member/login")
     R login(@RequestBody UserLoginVo vo);
    @PostMapping("member/member/oauth/login")
      R oauthlogin(@RequestBody SocialUserVo svo) throws Exception;
}
