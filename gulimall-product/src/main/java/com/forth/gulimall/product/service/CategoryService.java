package com.forth.gulimall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.forth.common.utils.PageUtils;
import com.forth.gulimall.product.entity.CategoryEntity;
import com.forth.gulimall.product.vo.Catelog2Vo;

import java.util.List;
import java.util.Map;

/**
 * 商品三级分类
 *
 * @author fourth-miaowei
 * @email 2608957980@qq.com
 * @date 2020-11-11 09:34:16
 */
public interface CategoryService extends IService<CategoryEntity> {

    PageUtils queryPage(Map<String, Object> params);

    List<CategoryEntity> listWithTree();

    void removeMenuByIds(List<Long> asList);

    //找到catelogId的完整路径 [父/子/孙]
    Long[] findCatelogPath(Long catelogId);

    void updateCascade(CategoryEntity category);

    List<CategoryEntity> getLevel1Category();

    Map<String, List<Catelog2Vo>> getCatalogJson();

}

