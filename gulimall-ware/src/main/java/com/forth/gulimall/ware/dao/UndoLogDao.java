package com.forth.gulimall.ware.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.forth.gulimall.ware.entity.UndoLogEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author chenang
 * @email chenang@gmail.com
 * @date 2020-11-11 21:16:45
 */
@Mapper
public interface UndoLogDao extends BaseMapper<UndoLogEntity> {
	
}
