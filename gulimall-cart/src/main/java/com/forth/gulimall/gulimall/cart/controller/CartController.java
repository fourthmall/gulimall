package com.forth.gulimall.gulimall.cart.controller;


import com.forth.gulimall.gulimall.cart.interceptor.CartInterceptor;
import com.forth.gulimall.gulimall.cart.vo.UserInfoVo;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;



@Controller
public class CartController {
@GetMapping("/cart.html")
public  String cartListPage(){

      UserInfoVo userInfoVo = CartInterceptor.threadLocal.get();
      System.out.println("--------------"+userInfoVo);

      return "cartList";
}
}
