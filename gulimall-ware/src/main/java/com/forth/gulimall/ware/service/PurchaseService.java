package com.forth.gulimall.ware.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.forth.common.utils.PageUtils;
import com.forth.gulimall.ware.entity.PurchaseEntity;
import com.forth.gulimall.ware.vo.MergeVo;
import com.forth.gulimall.ware.vo.PurchaseDoneVo;

import java.util.List;
import java.util.Map;

/**
 * 采购信息
 *
 * @author chenang
 * @email chenang@gmail.com
 * @date 2020-11-11 21:16:45
 */
public interface PurchaseService extends IService<PurchaseEntity> {

    PageUtils queryPage(Map<String, Object> params);
    //未领取采购单
    PageUtils queryPageUnreceivePurchase(Map<String, Object> params);
    //合并采购需求
    void mergePurchase(MergeVo mergeVo);
    //领取采购单
    void received(List<Long> ids);
    //完成采购单
    void done(PurchaseDoneVo doneVo);
}

