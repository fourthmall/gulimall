package com.forth.gulimall.member.dao;

import com.forth.gulimall.member.entity.MemberLevelEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 会员等级
 * 
 * @author fourth-miaowei
 * @email 2608957980@qq.com
 * @date 2020-11-12 09:30:55
 */
@Mapper
public interface MemberLevelDao extends BaseMapper<MemberLevelEntity> {


    MemberLevelEntity getDefaultLevel();
}
