package com.forth.gulimall.coupon.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.forth.common.utils.PageUtils;
import com.forth.gulimall.coupon.entity.SeckillSessionEntity;

import java.util.Map;

/**
 * 秒杀活动场次
 *
 * @author fourth-miaowei
 * @email 2608957980@qq.com
 * @date 2020-11-12 10:16:49
 */
public interface SeckillSessionService extends IService<SeckillSessionEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

