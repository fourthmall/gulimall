package com.forth.gulimall.gulimallauthserver.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.forth.common.constant.AuthServerConstant;
import com.forth.common.utils.R;
import com.forth.gulimall.gulimallauthserver.feign.MemberFeignService;
import com.forth.gulimall.gulimallauthserver.vo.MemberRespVo;
import com.forth.gulimall.gulimallauthserver.vo.SocialUserVo;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import com.forth.gulimall.gulimallauthserver.utils.HttpUtils;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;
@Slf4j
@Controller
public class OAuth2Controller {

    @Autowired
    MemberFeignService memberFeignService;

    @GetMapping("/oauth2.0/weibo/success")
    public  String weibo (@RequestParam("code") String code, HttpSession session, HttpServletResponse servletResponse) throws Exception {
        Map<String, String> map = new HashMap<>();
        map.put("client_id", "2789270821");
        map.put("client_secret", "cc3a93781eb4593907325e4c50e31eae");
        map.put("grant_type", "authorization_code");
        map.put("redirect_uri", "http://localhost:20000/oauth2.0/weibo/success");
        map.put("code", code);
        System.out.println("----" + map);

        Map<String, String> map2 = new HashMap<>();
        map.put("Cookie", "JSESSIONID=C3EBCF908AC5BF68C3380F0612C2D943");
        map.put("Cache-Control", "Jno-cache");
        map.put("Postman-Token", "<calculated when request is sent>");

        //1,code换取 accessToke"
        HttpResponse response = HttpUtils.doPost("https://api.weibo.com", "/oauth2/access_token", "post", map2, null, map);

        System.out.println("------------->>code换取 "+response.getStatusLine().getStatusCode());
        //2,处理
        if (response.getStatusLine().getStatusCode() == 200) {
            //获取 accessToke
            String json = EntityUtils.toString(response.getEntity());
            System.out.println("-------" + json.toString());
            SocialUserVo socialUserVo = JSON.parseObject(json, SocialUserVo.class);
            System.out.println("-------" + socialUserVo);

            //知道是呢个用户
            //如果第一次登录，自动祖册
            //登录或者注册
            ///问题
            R oauthlogin = memberFeignService.oauthlogin(socialUserVo);
            if (oauthlogin.get("code").equals(0)) {
                MemberRespVo data = oauthlogin.getData("data", new TypeReference<MemberRespVo>() {
                });
                System.out.println("登录成功：用户信息"+data);
                session.setAttribute(AuthServerConstant.LOGIN_USER,data);
                System.out.println(session.getAttribute(AuthServerConstant.LOGIN_USER));
//TODO 1,默认发的令牌 key seeion  值  是唯一字符串  作用域 解决不了子域的session
//TODO 2,使用json 的序列画 来存储到redis
               // servletResponse.addCookie("JSESSIONID","dadaa");
               // log.info("登录成功：用户信息",data.toString());
                //2,成功挑转首页
                return "redirect:http://localhost:30000/";
            } else {

            }

        } else {
            return "redirect:http://localhost:20000/index.html";
        }
return "";
    }
}

