package com.forth.gulimall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.forth.common.utils.PageUtils;
import com.forth.gulimall.product.entity.ProductAttrValueEntity;

import java.util.List;
import java.util.Map;

/**
 * spu属性值
 *
 * @author fourth-miaowei
 * @email 2608957980@qq.com
 * @date 2020-11-11 09:34:16
 */
public interface ProductAttrValueService extends IService<ProductAttrValueEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void saveProductAttr(List<ProductAttrValueEntity> collect);

    List<ProductAttrValueEntity> baseAttrlistforspu(Long spuId);

    void updateSpuAttr(Long spuId, List<ProductAttrValueEntity> entities);
}

