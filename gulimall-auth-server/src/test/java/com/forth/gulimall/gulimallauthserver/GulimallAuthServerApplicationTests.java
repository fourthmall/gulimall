package com.forth.gulimall.gulimallauthserver;

import com.forth.common.constant.AuthServerConstant;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.StringRedisTemplate;
import com.forth.gulimall.gulimallauthserver.utils.HttpUtils;


import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

@SpringBootTest
class GulimallAuthServerApplicationTests {
	@Autowired
	StringRedisTemplate redisTemplate;
	@Test
	void contextLoads() {

		String code = UUID.randomUUID().toString().substring(0, 5)+"_"+System.currentTimeMillis();

		String  phone ="17634362804";
		//2，验证码的再次验证  存 key -phone value-code
		//redis 缓存
		redisTemplate.opsForValue().set(AuthServerConstant.SMS_CODE_CACHE_PREFIX+phone,code,1000, TimeUnit.MINUTES);

	}
	@Test
	public void send() { 
			String host = "https://aliapi.market.alicloudapi.com";
			String path = "/smsApi/verifyCode/send";
			String method = "POST";
			String appcode = "0995740d4ffe4f64bd63dcea04160b07";
			Map<String, String> headers = new HashMap<String, String>();
			//最后在header中的格式(中间是英文空格)为Authorization:APPCODE 83359fd73fe94948385f570e3c139105
			headers.put("Authorization", "APPCODE " + appcode);
			Map<String, String> querys = new HashMap<String, String>();
			querys.put("phone", "17634362804");
			querys.put("templateId", "540");
			querys.put("variables", "12345");
			Map<String, String> bodys = new HashMap<String, String>();


			try {
				/**
				 * 重要提示如下:
				 * HttpUtils请从
				 * https://github.com/aliyun/api-gateway-demo-sign-java/blob/master/src/main/java/com/aliyun/api/gateway/demo/util/HttpUtils.java
				 * 下载
				 *
				 * 相应的依赖请参照
				 * https://github.com/aliyun/api-gateway-demo-sign-java/blob/master/pom.xml
				 */
				HttpResponse response = HttpUtils.doPost(host, path, method, headers, querys, bodys);
				System.out.println(response.toString());
				//获取response的body
				//System.out.println(EntityUtils.toString(response.getEntity()));
			} catch (Exception e) {
				e.printStackTrace();
			}}}

