package com.forth.gulimall.ware.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.forth.common.utils.PageUtils;
import com.forth.gulimall.ware.entity.WareInfoEntity;

import java.util.Map;

/**
 * 仓库信息
 *
 * @author chenang
 * @email chenang@gmail.com
 * @date 2020-11-11 21:16:45
 */
public interface WareInfoService extends IService<WareInfoEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

