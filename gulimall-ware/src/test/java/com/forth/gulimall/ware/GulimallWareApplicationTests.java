package com.forth.gulimall.ware;

import com.forth.gulimall.ware.entity.WareInfoEntity;
import com.forth.gulimall.ware.service.WareInfoService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class GulimallWareApplicationTests {

	@Autowired
	WareInfoService wareInfoService;

	@Test
	void contextLoads() {
		WareInfoEntity wareInfoEntity = new WareInfoEntity();
		wareInfoEntity.setName("测试");
		wareInfoService.save(wareInfoEntity);
		System.out.println("success");
	}

}
