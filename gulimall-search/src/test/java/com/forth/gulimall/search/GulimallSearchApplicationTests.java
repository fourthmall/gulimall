package com.forth.gulimall.search;

import com.alibaba.fastjson.JSON;
import com.forth.gulimall.search.config.GulimallElasticSearchConfig;
import lombok.Data;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.terms.TermsAggregationBuilder;
import org.elasticsearch.search.aggregations.metrics.AvgAggregationBuilder;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;

@RunWith(SpringRunner.class)
@SpringBootTest
class GulimallSearchApplicationTests {

	@Autowired
	private RestHighLevelClient client;

	@Test
	public void searchData() throws IOException {
		//1.创建检索请求
		SearchRequest searchRequest = new SearchRequest();
		//指定索引
		searchRequest.indices("bank");
		//指定DSL，检索条件
		SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
		//1.1)构造检索条件
		sourceBuilder.query(QueryBuilders.matchQuery("address","mill"));

		//1.2)按照年龄的值分布进行聚合
		TermsAggregationBuilder ageAgg = AggregationBuilders.terms("ageAgg").field("age").size(10);
		sourceBuilder.aggregation(ageAgg);

		//1.3)计算平均薪资
		AvgAggregationBuilder balanceAvg = AggregationBuilders.avg("balanceAvg").field("balance");
		sourceBuilder.aggregation(balanceAvg);

		//打印检索条件
		System.out.println("检索条件:"+sourceBuilder.toString());

		searchRequest.source(sourceBuilder);

		//2.执行检索
		SearchResponse searchResponse = client.search(searchRequest, GulimallElasticSearchConfig.COMMON_OPTIONS);

		//3.分析结果
		System.out.println(searchResponse.toString());
		//3.1)获取所有查到的数据
		SearchHits hits = searchResponse.getHits();
		SearchHit[] searchHits = hits.getHits();
		for (SearchHit hit : searchHits) {
			String string = hit.getSourceAsString();
		}
	}

	/**
	 * 测试存储数据到es
	 */
	@Test
	public void indexData() throws IOException {
		IndexRequest indexRequest = new IndexRequest("users");
		indexRequest.id("1");//数据的id

		indexRequest.source("user","zhangsan","age","18","gender","男");//要保存的内容

		//执行操作
		IndexResponse index = client.index(indexRequest, GulimallElasticSearchConfig.COMMON_OPTIONS);

		//提取有用的响应数据
		System.out.println(index);
	}


	@Data
	private static class User{
		private String userName;
		private String gender;
		private Integer age;
	}

	@Test
	public void contextLoads() {
		System.out.println(client);
	}

}
