package com.forth.gulimall.ware.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.forth.gulimall.ware.entity.PurchaseEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 采购信息
 * 
 * @author chenang
 * @email chenang@gmail.com
 * @date 2020-11-11 21:16:45
 */
@Mapper
public interface PurchaseDao extends BaseMapper<PurchaseEntity> {
	
}
