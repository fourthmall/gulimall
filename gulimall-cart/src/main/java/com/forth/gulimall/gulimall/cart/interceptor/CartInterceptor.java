package com.forth.gulimall.gulimall.cart.interceptor;

import com.forth.common.constant.AuthServerConstant;
import com.forth.common.constant.CartConstant;
import com.forth.common.vo.MemberRespVo;
import com.forth.gulimall.gulimall.cart.vo.UserInfoVo;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * 在执行目标方法判断用户的登录状态 封装传递给controller 目标请求
 */

public class CartInterceptor implements HandlerInterceptor {
    public static ThreadLocal<UserInfoVo> threadLocal=new ThreadLocal<>();

    /**
     * 目标方法执行之前
     *
     * @param request
     * @param response
     * @param handler
     * @return
     * @throws Exception
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {



        UserInfoVo userInfoVo = new UserInfoVo();
        HttpSession session = request.getSession();
        MemberRespVo attribute = (MemberRespVo) session.getAttribute(AuthServerConstant.LOGIN_USER);

        if (attribute != null) {
            userInfoVo.setUserId(attribute.getId());
        } else {

        }

        Cookie[] cookies = request.getCookies();
        if(cookies!=null &&cookies.length>0){
            for (Cookie cookie : cookies) {
                //user-key
                String name = cookie.getName();
                if (name.equals("GULISESSION")) {
                    System.out.println("-----------");
                }
                if (name.equals(CartConstant.TEMP_USER_COOKIE_NAME)){
                    userInfoVo.setUserKey(cookie.getValue());
                }
            }
        }


        //目标方法执行之前
         threadLocal.set(userInfoVo);
        return true;
    }
}
