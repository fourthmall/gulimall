package com.forth.gulimall.member.controller;

import java.util.Arrays;
import java.util.Map;

//import org.apache.shiro.authz.annotation.RequiresPermissions;
import com.forth.common.exception.BizCodeEnume;
import com.forth.gulimall.member.exception.PhoneExistException;
import com.forth.gulimall.member.exception.UsernameExistException;
import com.forth.gulimall.member.feign.CouponFeignService;
import com.forth.gulimall.member.vo.MemberLoginVo;
import com.forth.gulimall.member.vo.MemberRegistVo;
import com.forth.gulimall.member.vo.SocialUserVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.forth.gulimall.member.entity.MemberEntity;
import com.forth.gulimall.member.service.MemberService;
import com.forth.common.utils.PageUtils;
import com.forth.common.utils.R;


/**
 * 会员
 *
 * @author fourth-miaowei
 * @email 2608957980@qq.com
 * @date 2020-11-12 09:30:56
 */
@RestController
@RequestMapping("member/member")
public class MemberController {
    @Autowired
    private MemberService memberService;

    @Autowired
    CouponFeignService couponFeignService;

    @RequestMapping("/coupons")
    public R test() {
        MemberEntity memberEntity = new MemberEntity();
        memberEntity.setNickname("张三");

        R membercoupon = couponFeignService.membercoupon();

        return R.ok().put("member", memberEntity).put("coupons", membercoupon.get("coupons"));
    }

    /**
     * 社交登录
     * @param
     * @return
     */
    @PostMapping("/oauth/login")
    public  R oauthlogin(@RequestBody SocialUserVo svo) throws Exception {
        MemberEntity m=   memberService.login(svo);
        System.out.println("----0+"+m);

        if(m!=null){
            return R.ok().setData(m);
        }else {
            return  R.error(BizCodeEnume.LOGINACCT_PASSWORD_EXCEPTION.getCode(),BizCodeEnume.LOGINACCT_PASSWORD_EXCEPTION.getMsg());
        }

    }

    /**
     * 登录
     * @param vo 参数
     * @return
     */
    @PostMapping("/login")
    public  R login(@RequestBody MemberLoginVo vo){
     MemberEntity m=   memberService.login(vo);
        System.out.println("----0+"+m);

        if(m!=null){
         return R.ok().setData(m);
     }else {
         return  R.error(BizCodeEnume.LOGINACCT_PASSWORD_EXCEPTION.getCode(),BizCodeEnume.LOGINACCT_PASSWORD_EXCEPTION.getMsg());
     }

    }

    @PostMapping("/regist")
    public R regist(@RequestBody MemberRegistVo vo) {
        try {

            memberService.regist(vo);
        }catch (PhoneExistException e){
            return R.error(15200,"手机号存在");
        }catch (UsernameExistException e){
            return R.error(15100,"用户名存在");
        }

       return R.ok();

    }

    /**
     * 列表
     */
    @RequestMapping("/list")
    //@RequiresPermissions("member:member:list")
    public R list(@RequestParam Map<String, Object> params) {
        PageUtils page = memberService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    //@RequiresPermissions("member:member:info")
    public R info(@PathVariable("id") Long id) {
        MemberEntity member = memberService.getById(id);

        return R.ok().put("member", member);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    //@RequiresPermissions("member:member:save")
    public R save(@RequestBody MemberEntity member) {
        memberService.save(member);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    //@RequiresPermissions("member:member:update")
    public R update(@RequestBody MemberEntity member) {
        memberService.updateById(member);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    //@RequiresPermissions("member:member:delete")
    public R delete(@RequestBody Long[] ids) {
        memberService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
