package com.forth.gulimall.order;

import com.forth.gulimall.order.entity.OrderEntity;
import com.forth.gulimall.order.service.OrderService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Date;

@SpringBootTest
class GulimallOrderApplicationTests {

	@Autowired
	OrderService orderService;

	@Test
	void contextLoads() {
		OrderEntity orderEntity = new OrderEntity();
		orderEntity.setCreateTime(new Date());
		orderService.save(orderEntity);
		System.out.println("success");
	}

}
