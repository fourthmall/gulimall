package com.forth.gulimall.product;

import com.forth.gulimall.product.dao.AttrGroupDao;
import com.forth.gulimall.product.entity.BrandEntity;
import com.forth.gulimall.product.service.AttrGroupService;
import com.forth.gulimall.product.service.BrandService;
import com.forth.gulimall.product.vo.SpuItemAttrGroupVo;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.List;

@SpringBootTest
class GulimallProductApplicationTests {

	@Autowired
	BrandService brandService;

	@Autowired
    AttrGroupService attrGroupService;

    @Test
    public void test(){
        List<SpuItemAttrGroupVo> attrGroupWithAttrsBySpuId = attrGroupService.getAttrGroupWithAttrsBySpuId(5L, 225L);
        System.out.println(attrGroupWithAttrsBySpuId);
    }




}
