package com.forth.gulimall.member.dao;

import com.forth.gulimall.member.entity.MemberEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 会员
 * 
 * @author fourth-miaowei
 * @email 2608957980@qq.com
 * @date 2020-11-12 09:30:56
 */
@Mapper
public interface MemberDao extends BaseMapper<MemberEntity> {
	
}
