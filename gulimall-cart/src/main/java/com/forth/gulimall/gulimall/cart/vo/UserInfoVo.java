package com.forth.gulimall.gulimall.cart.vo;

import lombok.Data;
import lombok.ToString;


@Data
public class UserInfoVo {

    private Long userId;
    private  String userKey;
}
