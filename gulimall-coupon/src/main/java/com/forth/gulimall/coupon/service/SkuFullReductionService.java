package com.forth.gulimall.coupon.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.forth.common.to.SkuReductionTo;
import com.forth.common.utils.PageUtils;
import com.forth.gulimall.coupon.entity.SkuFullReductionEntity;

import java.util.Map;

/**
 * 商品满减信息
 *
 * @author fourth-miaowei
 * @email 2608957980@qq.com
 * @date 2020-11-12 10:16:49
 */
public interface SkuFullReductionService extends IService<SkuFullReductionEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void saveSkuReduction(SkuReductionTo skuReductionTo);
}

