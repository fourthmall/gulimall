package com.forth.gulimall.product.vo;

import com.forth.gulimall.product.entity.SkuImagesEntity;
import com.forth.gulimall.product.entity.SkuInfoEntity;
import com.forth.gulimall.product.entity.SpuInfoDescEntity;
import lombok.Data;
import lombok.ToString;

import java.util.List;

@Data
@ToString
public class SkuItemVo {
    // sku基本信息的搜索
    SkuInfoEntity info;

    boolean hasStock = true;
    //图片信息
    List<SkuImagesEntity> images;
    //获取SPu销售组合
    List<SkuItemSaleAttrVo> saleAttr;
    //获取spu介绍
    SpuInfoDescEntity desp;
    //获取spu规格参数信息
    List<SpuItemAttrGroupVo> groupAttrs;

    SeckillInfoVo seckillInfo; //当前商品的秒杀优惠信息

}
