package com.forth.gulimall.member.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.forth.common.utils.HttpUtils;
import com.forth.gulimall.member.dao.MemberLevelDao;
import com.forth.gulimall.member.entity.MemberLevelEntity;
import com.forth.gulimall.member.exception.PhoneExistException;
import com.forth.gulimall.member.exception.UsernameExistException;
import com.forth.gulimall.member.vo.MemberLoginVo;
import com.forth.gulimall.member.vo.MemberRegistVo;
import com.forth.gulimall.member.vo.SocialUserVo;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.forth.common.utils.PageUtils;
import com.forth.common.utils.Query;

import com.forth.gulimall.member.dao.MemberDao;
import com.forth.gulimall.member.entity.MemberEntity;
import com.forth.gulimall.member.service.MemberService;


@Service("memberService")
public class MemberServiceImpl extends ServiceImpl<MemberDao, MemberEntity> implements MemberService {

    @Autowired
    MemberLevelDao memberLevelDao;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<MemberEntity> page = this.page(
                new Query<MemberEntity>().getPage(params),
                new QueryWrapper<MemberEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public void regist(MemberRegistVo vo) {
        MemberDao baseMapper = this.baseMapper;
        MemberEntity entity = new MemberEntity();
        MemberLevelEntity levelEntity = memberLevelDao.getDefaultLevel();
        //设置默认等级
        entity.setLevelId(levelEntity.getId());
        entity.setMobile(vo.getPhone());

        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String encode = passwordEncoder.encode(vo.getPassword());

        entity.setPassword(encode);
        //检查用户名和手机是否唯一  为了
        checkPhoneUnique(vo.getPhone());
        checkUsernameUnique(vo.getUserName());


        entity.setUsername(vo.getUserName());
        baseMapper.insert(entity);
    }

    @Override
    public void checkPhoneUnique(String phone) throws PhoneExistException {
        MemberDao baseMapper = this.baseMapper;
        Integer mobile = baseMapper.selectCount(new QueryWrapper<MemberEntity>().eq("mobile", phone));
        if (mobile > 0) {
            throw new PhoneExistException();
        }


    }

    @Override
    public void checkUsernameUnique(String Username) throws UsernameExistException {

        Integer mobile = baseMapper.selectCount(new QueryWrapper<MemberEntity>().eq("username", Username));
        if (mobile > 0) {
            throw new UsernameExistException();

        }

    }

    @Override
    public MemberEntity login(MemberLoginVo vo) {
        MemberDao baseMapper = this.baseMapper;
        String loginacct = vo.getLoginacct();
        String password = vo.getPassword();

        //1,去数据库查询
        MemberEntity entity = baseMapper.selectOne(new QueryWrapper<MemberEntity>().eq("username", loginacct).or()
                .eq("mobile", loginacct));
       // System.out.println("1,--------" + entity.toString());
        if (entity == null) {
            //失败
            return null;
        } else {
            //获取密码
            String password1 = entity.getPassword();
            BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
            //密码是否匹配
            boolean matches = passwordEncoder.matches(password, password1);
            //entity.get();
            if (matches) {
                return entity;
            } else {
                return null;
            }
        }

    }

    @Override
    public MemberEntity login(SocialUserVo svo) throws Exception {

        String uid = svo.getUid();
        MemberDao baseMapper = this.baseMapper;
        MemberEntity memberEntity = baseMapper.selectOne(new QueryWrapper<MemberEntity>().eq("social_uid", uid));
        if (memberEntity != null) {
            //用户
            System.out.println("登录");
            System.out.println("---11-----");
            MemberEntity update = new MemberEntity();
            update.setId(memberEntity.getId());
            update.setAccessToken(svo.getAccess_token());
            update.setExpiresIn(svo.getExpires_in());

            baseMapper.updateById(update);

            memberEntity.setAccessToken(svo.getAccess_token());
            memberEntity.setExpiresIn(svo.getExpires_in());
            return memberEntity;

        } else {
            //没有查到当前社交用户记录，就要我们注册一个
            System.out.println("注册");
            System.out.println("--------");
            MemberEntity mem = new MemberEntity();
            try {
                System.out.println("--------");
                Map<String, String> map2 = new HashMap<>();
                map2.put("Cookie", "JSESSIONID=C3EBCF908AC5BF68C3380F0612C2D943");
                map2.put("Cache-Control", "Jno-cache");
                map2.put("Postman-Token", "<calculated when request is sent>");


                Map<String, String> map = new HashMap<>();
                map.put("access_token", svo.getAccess_token());
                map.put("uid", svo.getUid());
                System.out.println("map"+map);
                //3,查询当前的社交账号的基本信息
                HttpResponse response = HttpUtils.doGet("https://api.weibo.com", "/2/users/show.json", "get", map2, map);

                System.out.println("状态码--------》"+response.getStatusLine().getStatusCode());
                if (response.getStatusLine().getStatusCode() == 200) {
                    //查询成功
                    String json = EntityUtils.toString(response.getEntity());
                    JSONObject jsonObject = JSON.parseObject(json);

                    System.out.println("------------>>"+jsonObject);
                    //昵称
                    String name = jsonObject.getString("name");
                    //性别
                    String sex = jsonObject.getString("gender");
                    System.out.println("查询的数据---》》"+name+sex);
                    //...
                    mem.setUsername(name);
                    mem.setGender("m".equals(sex) ? 1 : 0);
                    //...
                    mem.setSocialUid(svo.getUid());
                    mem.setAccessToken(svo.getAccess_token());
                    mem.setExpiresIn(svo.getExpires_in());

                }
            }catch (Exception e){}
            System.out.println("--------");
            mem.setSocialUid(svo.getUid());
            mem.setAccessToken(svo.getAccess_token());
            mem.setExpiresIn(svo.getExpires_in());
            baseMapper.insert(mem);
            //登录或者祖册
            return mem;
        }

    }

}