package com.forth.gulimall.order.dao;

import com.forth.gulimall.order.entity.OrderReturnApplyEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 订单退货申请
 * 
 * @author fourth-miaowei
 * @email 2608957980@qq.com
 * @date 2020-11-12 09:13:18
 */
@Mapper
public interface OrderReturnApplyDao extends BaseMapper<OrderReturnApplyEntity> {
	
}
