package com.forth.gulimall.product.dao;

import com.forth.gulimall.product.entity.BrandEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 品牌
 * 
 * @author fourth-miaowei
 * @email 2608957980@qq.com
 * @date 2020-11-11 09:34:16
 */
@Mapper
public interface BrandDao extends BaseMapper<BrandEntity> {
	
}
