package com.forth.gulimall.coupon.dao;

import com.forth.gulimall.coupon.entity.CouponEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 优惠券信息
 * 
 * @author fourth-miaowei
 * @email 2608957980@qq.com
 * @date 2020-11-12 10:16:50
 */
@Mapper
public interface CouponDao extends BaseMapper<CouponEntity> {
	
}
